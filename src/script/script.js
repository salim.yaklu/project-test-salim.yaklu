// Hide Header when scrolling
const header = document.getElementById('header');
let lastScrollTop = 0;

window.addEventListener('scroll', function () {
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;

    if (scrollTop > lastScrollTop) {
        // Scroll down
        header.style.top = '-100px'; // Hide header
    } else {
        // Scroll up
        header.style.top = '0'; // Show header
        header.style.backgroundColor = `rgba(255, 103, 0, 0.7)`;

        if (scrollTop == 0) {
            header.style.backgroundColor = '#FF6700';
        }
    }

    lastScrollTop = scrollTop;
});


// Fetch API & data listing
document.addEventListener("DOMContentLoaded", function () {
    // URL API
    const apiUrl = "https://suitmedia-backend.suitdev.com/api/ideas";
    var ideas = [];

    // Parameter API
    const params = {
        "page[number]": 1,
        "page[size]": 10,
        "append": ['small_image', 'medium_image'],
        "sort": 'published_at'
    };

    // Membuat URL dengan parameter
    const urlWithParams = new URL(apiUrl);
    Object.keys(params).forEach(key => urlWithParams.searchParams.append(key, params[key]));

    // Mengambil data dari API dengan menambahkan header JSON
    fetch(urlWithParams, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`)
            }
            return response.json();
        })
        .then(data => {
            console.log(data.data);

            // Push data.data ke array ideas
            ideas.push(data.data);
            console.log(ideas);

            // Pagination
            

            // Memastikan bahwa data dan data.data tersedia
            if (data && data.data && Array.isArray(data.data)) {
                // Menampilkan data di dalam elemen dengan id "ideas-container"
                const ideasContainer = document.getElementById("ideas-container");

                    // Menggunakan map untuk membuat elemen HTML dari data
                    data.data.forEach(idea => {
                        // Check if medium_image is defined and has the 'url' property
                        const imageUrl = idea.medium_image && idea.medium_image.length > 0 && idea.medium_image[0].url;

                        ideasContainer.innerHTML += `
                    <div class="max-w-xs w-full sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/4 me-3 mb-6 rounded-lg shadow">
                        <div class="h-48 overflow-hidden">
                            <img class="rounded-t-lg object-cover h-full w-full" src="${imageUrl || 'placeholder.jpg'}" alt="" />
                        </div>
                        <div class="p-5 flex flex-col">
                            <a href="#">
                                <h4 class="text-gray">${formatDate(idea.published_at)}</h4>
                                <h5 class="mb-2 text-xl font-bold tracking-tighter line-clamp-3">${idea.title}</h5>
                            </a>
                        </div>
                    </div>
                    `;
                    });
            } else {
                console.error("Invalid data structure:", data);
            }
        })
        .catch(error => console.error("Error fetching data:", error));
});

function formatDate(inputDate) {
    const options = { day: 'numeric', month: 'long', year: 'numeric' };
    const formattedDate = new Date(inputDate).toLocaleDateString('id-ID', options);
    return formattedDate;
}


// Dropdown menu
const pageBtn = document.getElementById('page-btn');
const sortBtn = document.getElementById('sort-btn');
const pageField = document.getElementById('page-field');
const sortField = document.getElementById('sort-field');
const pageOption = document.getElementById('page-option');
const sortOption = document.getElementById('sort-option');

pageBtn.onclick = function() {
    pageField.classList.toggle('hidden');
}

sortBtn.onclick = function() {
    sortField.classList.toggle('hidden');
}

for(option of pageOption) {
    option.onclick = function() {
        const selectedValue = this.textContent.trim();
        const itemsPerPage = parseInt(selectedValue); 

    }
}

for(option of sortOption) {
    option.onclick = function() {
        const selectedValue = this.textContent.trim();
        const itemsPerPage = parseInt(selectedValue); 

    }
}